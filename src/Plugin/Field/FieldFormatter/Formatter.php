<?php

/**
 * @file
 * Contains \Drupal\formatter\Plugin\field\formatter\Formatter.
 */

namespace Drupal\formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;


/**
 * Plugin implementation of the 'text_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "text_field_formatter",
 *   label = @Translation("Text field formatter"),
 *   field_types = {
 *     "string",
 *   }
 * )
 */

class Formatter extends FormatterBase
{
    /**
     * {@inheritdoc}
     */
    public function viewElements(FieldItemListInterface $items, $langcode)
    {
        $elements = array();

         foreach ($items as $delta => $item) {
            $elements[$delta] = array(
                '#theme' => 'text_formatter',
                '#value' => $item->value,
            );
      

        

        return $elements;
    }


}
}
