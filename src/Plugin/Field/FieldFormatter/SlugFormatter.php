<?php

namespace Drupal\formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\StringFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Component\Utility\Html;

/**
 * Plugin implementation of the 'text_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "text_field_slug_formatter",
 *   label = @Translation("Text field slug formatter"),
 *   field_types = {
 *     "string",
 *   },
 *   edit = {
 *     "editor" = "form"
 *   },
 *   quickedit = {
 *     "editor" = "plain_text"
 *   }
 * )
 */
class SlugFormatter extends StringFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'wrap_tag' => '-',
      'wrap_class' => '',
      'wrap_attributes' => '',
      'override_link_label' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultWrapTagOptions() {
    $wrappers = [
      //'-' => t('-'),
      '%' => t('%'),
      '*' => t('*'),
      '$' => t('$'),
      '!' => t('!   '),
      '_' => t('_'),
      '@' => t('@'),
      '/' => t('/'),
    ];

    return $wrappers;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['wrap_tag'] = [
      '#title' => $this->t('Slug seperator'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('wrap_tag'),
      '#empty_option' => $this->t('-'),
      '#options' => $this->defaultWrapTagOptions(),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $wrap_tag = $this->getSetting('wrap_tag');
    if ('-' == $wrap_tag) {
      $summary[] = $this->t('No Slug tag defined. Default value is "-"');
    }
    else {
      $summary[] = $this->t('Slug text with tag: @tag', ['@tag' => $wrap_tag]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    
    $elements = parent::viewElements($items, $langcode);
    $wrap_tag = $this->getSetting('wrap_tag');

    foreach ($items as $delta => $item) {
      if ($wrap_tag !== '' && $elements[$delta]["#type"] == 'link') {
        $str = $elements[$delta]["#title"]["#context"]["value"];
        // Implementing drupal services
        $our_service = \Drupal::service('formatter.get_slug_data');
        $temp = $our_service-> get_slug_data($str,$wrap_tag);
        
        $elements[$delta]["#title"]["#context"]["value"] = [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#value' => $temp,
        ];
        unset($temp);
      }
      elseif ($wrap_tag !== '' && $elements[$delta]["#type"] != 'link') {
        $str = $item->value;
        // Implementing drupal services
        $our_service = \Drupal::service('formatter.get_slug_data');
        $slug_val = $our_service-> get_slug_data($str,$wrap_tag);
        $elements[$delta] = [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#value' => $slug_val,
        ];
      }
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return array
   *   The textual output generated as a render array.
   */
  protected function viewValue(FieldItemInterface $item) {
    // The text value has no text format assigned to it, so the user input
    // should equal the output, including newlines.
    return [
      '#type' => 'inline_template',
      '#template' => '{{ value }}',
      '#context' => ['value' => $item->value],
    ];
  }

 


}
