<?php

/**
 * @file
 * Contains \Drupal\formatter\SlugData.
 */

namespace Drupal\formatter;

use Drupal\taxonomy\Entity\Term;
use Drupal\node\Entity\Node;

/**
 * Defines SlugData service.
 */
class SlugData {

  // Get's the term name from tid
  public function get_slug_data($str = '', $wrap_tag = ''){
    $text_bundle = array();
    $text_bundle = explode(" ",$str);
    $temp = implode($wrap_tag,$text_bundle);
    return $temp;
  }

}
